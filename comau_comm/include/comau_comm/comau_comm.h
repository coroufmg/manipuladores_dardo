/**
* This file and related .cpp declare network interface functions
* to control the COMAU robot through TCP/IP. The message 
* format is defined in the PDL COMAU server and vary slighly between functions. 
* The method names match the ones used in PDL, therefore the user should refer
* to the COMAU documentation to know the expected behaviour. Please
* refer to the disclaimer and license as stated below.
*
* This library does not require ROS. For ROS related implementations,
* please refer to comau_ros.hpp.
*
* @author Murilo Marques Marinho
* @email  murilomarinho@lara.unb.br
* @since November 2014
*
************************************************************************
*
* Copyright (c) 2014, Murilo Marques Marinho
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
* 3. Neither the name of Murilo Marques Marinho nor the
*    names of its contributors may be used to endorse or promote products
*    derived from this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef COMAU_COMM_HEADER_GUARD
#define COMAU_COMM_HEADER_GUARD

//C++ Related
#include <iostream>
#include <exception>
#include <cmath>
#include <stdlib.h> //malloc, free, realloc
#include <stdio.h>
const double PI  = M_PI;
const double PI2 = PI/2;

//Eigen Related
#include <eigen3/Eigen/Dense>

//QT Related
#include <QTcpSocket>
#include <QtCore>

//Debug only
#include <iostream>

//For std::runtime_error
#include <stdexcept>

using namespace Eigen;


/*********************************
* ComauComm CLASS                *
**********************************/
class ComauComm
{
    public:
        QTcpSocket socket_;

        QString  hostname_;
        quint16  port_;
        int      timeout_;

        ComauComm()
        {
            hostname_ = QString("");
            port_     = 0;
            timeout_  = -1;
        }

        ComauComm(QString hostname,quint16 port,int timeout)
        {
            hostname_ = hostname;
            port_     = port;
            timeout_  = timeout;
        }

        ~ComauComm()
        {
            socketClose();
        }

        bool socketConnect();
        QByteArray socketRead();
        bool socketWrite(const char* command);
        void socketClose();

        QString getDataFromResponse(QByteArray response);

        void setArmOverride(int arm_override);
        void setTimeout(int timeout);
        void setJointPositions(Matrix<double,6,1> joint_vector);
        void setJointPositionsFly(Matrix<double,6,1> joint_vector);
        void Garra(bool pos);
	void tcpFlyMovement(Matrix<double,6,1> pos_vec);
	void setArmPose(Matrix<double,6,1> pos_vec);
	Matrix<double,6,1> getBasePose();

        Matrix<double,6,1> getJointPositions();

        void moveAboutBase(Vector3d vec, double angle);
 
        /*We need to overload the = operator because QTcpSocket does not have a copy
          implementation for some reason. Therefore we copy everything besides the
          socket from the other instance.*/
        ComauComm& operator= (const ComauComm& arg)
        {
            this->hostname_   = arg.hostname_;
            this->port_       = arg.port_;
            this->timeout_    = arg.timeout_;
            return *this;
        }         
};

double rad2deg(double rad);
double deg2rad(double deg);

#endif
