/**
*
* COMMAU Communication Node using QTcpSocket for TCP/IP communication.
*
************************************************************************
*
* Copyright (c) 2014, Murilo Marques Marinho
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
* 3. Neither the name of Murilo Marques Marinho nor the
     names of its contributors may be used to endorse or promote products
     derived from this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
* 
* The views and conclusions contained in the software and documentation are those
* of the authors and should not be interpreted as representing official policies, 
* either expressed or implied, of the FreeBSD Project.
*/

//Robot interface functions
#include <comau_comm/comau_comm.h>

/*********************************
*       TEST FUNCTIONS           *
**********************************/

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"

double posX, posY, posZ;

int test__simulatedProtocolTest(int argc, char** argv)
{
    //We need this for QTcpSocket to work.
    QCoreApplication app(argc, argv);

    ComauComm comau_comm("www.google.com.br",80,3000);
    comau_comm.socketConnect();
    comau_comm.socketWrite("GET /images/nav_logo195.png HTTP/1.0\r\n\r\n");
    QByteArray ba;
    ba = comau_comm.socketRead();

    try
    {
        comau_comm.getDataFromResponse("+ 10.0 20.0 30.0 40.0 60.0 70.0 80.0\n+\n");
    }
    catch (std::runtime_error& e)
    {
        std::cout << "RUNTIME EXCEPTION CAUGHT -- " << e.what() << std::endl;
    }

    comau_comm.setArmOverride(30);
    comau_comm.setTimeout(20000);
    VectorXd initialpos(6);
    initialpos << 0,0,-PI2,0,PI2,0;
    comau_comm.setJointPositions(initialpos);
    std::cout << " " << comau_comm.getJointPositions().transpose() << std::endl;

    Vector3d bla(1,2,3);
    double angle = PI2;
    //comau_comm.moveAboutBase(bla,angle);
    

    comau_comm.socketClose();

    return 0;
}

/*********************************
*      EXAMPLE FUNCTIONS         *
**********************************/

int example__initialPosition(int argc, char** argv)
{
    //We need this for QTcpSocket to work.
    QCoreApplication app(argc, argv);

    ComauComm comau_comm("172.22.121.2", 4121, 100);
    comau_comm.socketConnect();
    QByteArray response;
    comau_comm.getDataFromResponse(comau_comm.socketRead());

    comau_comm.setArmOverride(30);

    comau_comm.setTimeout(20000);

    VectorXd initialpos(6);
    initialpos << deg2rad(90),0,-PI2,0,PI2,0;
    comau_comm.setJointPositions(initialpos);

    std::cout << comau_comm.getJointPositions().transpose() << std::endl;

    comau_comm.socketClose();
}

/*********************************
*       Other Functions          *
**********************************/

void RI(double T[4][4], double R[3][3]){
	int i, j;
	
	for(i = 0; i < 3; i++){
		for(j =0; j < 3; j++){
			R[i][j] = T[j][i];
		}
	}
}

void transforma(double T[4][4], double teta, double x){
	T[0][0] = cos(teta);
	T[0][1] = -sin(teta);
	T[0][2] = 0;
	T[0][3] = x;

	T[1][0] = sin(teta);
	T[1][1] = cos(teta);
	T[1][2] = 0;
	T[1][3] = 0;
	
	T[2][0] = 0;
	T[2][1] = 0;
	T[2][2] = 1;
	T[2][3] = 0;
	
	T[3][0] = 0;
	T[3][1] = 0;
	T[3][2] = 0;
	T[3][3] = 1;
}

void produto(double t[4][4], double t2[4][4], double P[4][4]){
	int i, j;
	
	for(i = 0; i< 4; i++){
		for(j = 0; j< 4; j++){
			P[i][j] = t[i][0]*t2[0][j] + t[i][1]*t2[1][j] + t[i][2]*t2[2][j] + t[i][3]*t2[3][j];
		}
	}
}

void vetorial(double V1[3], double V2[3], double Vo[3]){
	Vo[0] = - (V1[1]*V2[2] - V1[2]*V2[1]);
	Vo[1] = - (V1[0]*V2[2] - V1[2]*V2[0]);
	Vo[2] = - (V1[0]*V2[1] - V1[1]*V2[0]);
	
	
}

void prodMV(double M[3][3], double V[3], double P[3]){
	int i, j;
	
	for(i = 0; i < 3; i++){
		P[i] = M[i][0]*V[0] + M[i][1]*V[1] + M[i][2]*V[2];
	}
}
void posvel(double J2e,double J3e, double J5e, double PV[6]){
	int i, j;
	double h = 450, x1 = 150, l0 = 590, l1 = 660, l2 = 95;
	double teta[3] = {deg2rad(160), deg2rad(170), 0};
	double J[3];
	double T10[4][4], T21[4][4], T32[4][4], T43[4][4], T40[4][4];
	double R10[3][3], R21[3][3], R32[3][3], R43[3][3];
	double P12[3] = {l0, 0, 0}, P23[3] = {l1, 0, 0}, P34[3] = {l2, 0, 0};
	double w11[3] = {0, 0, 0}, w22[3] = {0, 0, 0}, w33[3] = {0, 0, 0};
	double v11[3] = {0, 0, 0}, v22[3], v33[3], v44[3], v04[3];
	double temp[4][4], temp2[4][4], temp3[3], temp4[3][3];
	
	J[0] = J2e;
	J[1] = -J3e - deg2rad(11.36);
	J[2] = J5e + deg2rad(11.36);
	
	transforma(T10,J[0],h);
	transforma(T21,J[1],l0);
	transforma(T32,J[2],l1);
	transforma(T43,0,l2);
	
	produto(T10,T21,temp);
	produto(T32, T43, temp2);
	produto(temp, temp2, T40);
	
	PV[0] = T40[1][3] + x1;
	PV[1] = -T40[2][3];
	PV[2] = T40[0][3];
	
	RI(T10,R10);
	RI(T21,R21);
	RI(T32,R32);
	RI(T43,R43);
	
	w11[2] = teta[0];
	w22[2] = teta[0]+teta[1];
	w33[2] = teta[0]+teta[1]+teta[2];
		
	vetorial(w11,P12,temp3);
	temp3[0] = temp3[0] + v11[0];
	temp3[1] = temp3[1] + v11[1];
	temp3[2] = temp3[2] + v11[2];
	prodMV(R21,temp3,v22);
	
	vetorial(w22,P23,temp3);
	temp3[0] = temp3[0] + v22[0];
	temp3[1] = temp3[1] + v22[1];
	temp3[2] = temp3[2] + v22[2];
	prodMV(R32,temp3,v33);
	
	vetorial(w33,P34,temp3);
	temp3[0] = temp3[0] + v33[0];
	temp3[1] = temp3[1] + v33[1];
	temp3[2] = temp3[2] + v33[2];
	prodMV(R43,temp3,v44);
	
	for(i = 0; i < 3; i++){
		for(j =0; j < 3; j++){
			temp4[i][j] = T40[i][j];
		}
	}
	
	prodMV(temp4,v44,v04);
	
	PV[3] = v04[1];
	PV[4] = -v04[3];
	PV[5] = v04[0];
}


/* A fun��o planejamento devolve os pontos por onde o robo deve passar para jogar o dardo:
O rob� � tratado como tendo 4 graus de liberdade juntas 4 e 6 paradas em 0.
xalvo -> posi��o x em que o alvo est� em rela��o ao rob� no sistema de refer�ncia da base
yalvo -> posi��o y em que o alvo est� em rela��o ao rob� no sistema de refer�ncia da base
zalvo -> posi��o z em que o alvo est� em rela��o ao rob� no sistema de refer�ncia da base
n -> n�mero de pontos escolhido

*/
double *planejamento(double xalvo,double yalvo, double zalvo, int n)
{
  //z0-> altura da junta2 em rela��o a base l0->link entre a junta 2 e a junta 3 l1-> link entre a junta 3 e a junta 5
  double J0[6] = {0, deg2rad(-85), 0, 0, 0, 0}, dJ[6] = {0, deg2rad(1), -deg2rad(170/160), 0, 0, 0}, JS[6] =  {0, deg2rad(55), -deg2rad(146), 0, 0, 0};
  double *J;
  double sx, sz, sza , t, g = 9.8, erro = 20, z0 = 545, j[6], P[6];
  int i;
  
  j[0] = atan2(yalvo,xalvo);
  //if(j[0] > deg2rad(45))
//	j[0] = deg2rad(45);
 // if( j[0] < - deg2rad(45))
//	j[0] = -deg2rad(45);
  j[1] = J0[1];
  j[2] = J0[2];
  j[3] = J0[3];
  j[4] = J0[4];
  j[5] = J0[5];
  
  
  sx = sqrt(pow(xalvo, 2) + pow(yalvo,2));
  sz = zalvo - z0;
  sza = sz + 2*erro;
  
  while((sza - sz) > erro || (sza - sz) < - erro){
  	j[1] = j[1] + dJ[1];
  	j[2] = j[2] + dJ[2];
  
  	j[4] = 0;
  
 	posvel(j[1],j[2],j[4], P);
  
  	t = (sx - P[0])/P[3];
  	//std::cout<<"\np0\n"<<P[0];
  	//std::cout<<"\np3\n"<<P[3];
  	//std::cout<<"\nt\n"<<t;
  
  	sza = P[2] + P[5]*t - 0.5*g*pow(t, 2); 
  	
  	//std::cout<<"\np2\n"<<P[2];
  	//std::cout<<"\np5\n"<<P[5];
  	//std::cout<<"\nsza-sz\n"<<(sza - sz);
  }
  
  
  J = (double*) malloc(n * 6 * sizeof(double));
  if(J == NULL)                     
    {
        printf("Error! memory not allocated.");
        exit(0);
    }
  
   for(i = 0; i < n; i++){
  	if(i < (n/2 + 1)){
  		J[6*i] = j[0];
  		J[1 + 6*i] = J0[1] + (double)i * (j[1] - J0[1])/(n/2);
  		if(J[1 + 6*i] > JS[1])
  			J[1 + 6*i] = JS[1];
  		J[2 + 6*i] = J0[2] + (double)i * (j[2] - J0[2])/(n/2);
  		if(J[2 + 6*i] < JS[2])
  			J[2 + 6*i] = JS[2];
  		J[3 + 6*i] = 0;
  		//J[4 + 6*i] = J0[4] + (double)i * (j[4] - J0[4])/10;
		J[4 + 6*i] = j[4];
  		J[5 + 6*i] = deg2rad(-179);
	  }
	else{
  		J[6*i] = j[0];
  		J[1 + 6*i] = j[1] + ((double)i - n/2) * (j[1] - J0[1])/(n/2);
  		if(J[1 + 6*i] > JS[1])
  			J[1 + 6*i] = JS[1];
  		J[2 + 6*i] = j[2] + ((double)i - n/2) * (j[2] - J0[2])/(n/2);
  		if(J[2 + 6*i] < JS[2])
  			J[2 + 6*i] = JS[2];
  		J[3 + 6*i] = 0;
  		//J[4 + 6*i] = j[4] + ((double)i - 10) * (j[4] - J0[4])/10;
		J[4 + 6*i] = j[4];
  		J[5 + 6*i] = deg2rad(-179);
		}
	}
  return J;
}

void printpos(double *J, int n){
	int i, j;
	
	std::cout<<"\nPontos gerados:\n";
 	for(i = 0; i<n; i++){
 		for(j = 0; j<6; j++){
 			std::cout<<" "<< rad2deg(J[ 6*i + j]);
		 }
 		std::cout<<"\n";
	 }
}

void pegadardo(int n, ComauComm &comau_comm){

    	comau_comm.setArmOverride(50);
	bool pos_garra;
	VectorXd pos(6);	
	if(n == 1){
		pos << deg2rad(-28.501), deg2rad(81.545), deg2rad(-84.311), deg2rad(-29.364), deg2rad(102.219), deg2rad(-6.758);
		comau_comm.setJointPositions(pos);
		comau_comm.setArmOverride(40);
		pos_garra = true;
		comau_comm.Garra(pos_garra);
		pos << deg2rad(-31.712), deg2rad(78.152), deg2rad(-95.889), deg2rad(-31.978), deg2rad(94.894), deg2rad(-3.028);
		comau_comm.setJointPositions(pos);
		pos_garra = false;
		comau_comm.Garra(pos_garra);
		pos << deg2rad(-30.487), deg2rad(77.824), deg2rad(-97.208), deg2rad(-30.710), deg2rad(94.108), deg2rad(-2.411);
		comau_comm.setJointPositions(pos);
		pos << deg2rad(-28.501), deg2rad(81.545), deg2rad(-84.311), deg2rad(-29.364), deg2rad(102.219), deg2rad(-6.758);
		comau_comm.setJointPositions(pos);
	}
	if(n == 2){
		pos << deg2rad(-21.844), deg2rad(79.461), deg2rad(-91.065), deg2rad(-22.244), deg2rad(98.613), deg2rad(-3.448);
		comau_comm.setJointPositions(pos);
		comau_comm.setArmOverride(40);
		pos_garra = true;
		comau_comm.Garra(pos_garra);
		pos << deg2rad(-24.646), deg2rad(76.602), deg2rad(-102.786), deg2rad(-24.782), deg2rad(90.389), deg2rad(-0.133);
		comau_comm.setJointPositions(pos);
		pos_garra = false;
		comau_comm.Garra(pos_garra);
		pos << deg2rad(-23.679), deg2rad(76.474), deg2rad(-103.463), deg2rad(-23.814), deg2rad(89.891), deg2rad(0.099);
		comau_comm.setJointPositions(pos);
		pos << deg2rad(-21.844), deg2rad(79.461), deg2rad(-91.065), deg2rad(-22.244), deg2rad(98.613), deg2rad(-3.448);
		comau_comm.setJointPositions(pos);
	}	
	if(n == 3){
		pos << deg2rad(-16.047), deg2rad(78.382), deg2rad(-94.973), deg2rad(-16.281), deg2rad(96.215), deg2rad(-1.735);
		comau_comm.setJointPositions(pos);
		comau_comm.setArmOverride(40);
		pos_garra = true;
		comau_comm.Garra(pos_garra);
		pos << deg2rad(-18.229), deg2rad(75.939), deg2rad(-106.603), deg2rad(-18.384), deg2rad(87.421), deg2rad(0.926);
		comau_comm.setJointPositions(pos);
		pos_garra = false;
		comau_comm.Garra(pos_garra);
		pos << deg2rad(-17.429), deg2rad(75.885), deg2rad(-106.970), deg2rad(-17.588), deg2rad(87.112), deg2rad(0.986);
		comau_comm.setJointPositions(pos);
		pos << deg2rad(-16.047), deg2rad(78.382), deg2rad(-94.973), deg2rad(-16.281), deg2rad(96.215), deg2rad(-1.735);
		comau_comm.setJointPositions(pos);
	}	
	if(n == 4){
		pos << deg2rad(-10.243), deg2rad(77.697), deg2rad(-90.563), deg2rad(-10.411), deg2rad(94.495), deg2rad(-0.732);
		comau_comm.setJointPositions(pos);
		comau_comm.setArmOverride(40);
		pos_garra = true;
		comau_comm.Garra(pos_garra);
		pos << deg2rad(-11.665), deg2rad(75.581), deg2rad(-108.950), deg2rad(-11.839), deg2rad(85.398), deg2rad(1.053);
		comau_comm.setJointPositions(pos);
		pos_garra = false;
		comau_comm.Garra(pos_garra);
		pos << deg2rad(-10.084), deg2rad(75.533), deg2rad(-109.357), deg2rad(-10.258), deg2rad(85.021), deg2rad(0.994);
		comau_comm.setJointPositions(pos);
		pos << deg2rad(-10.243), deg2rad(77.697), deg2rad(-90.563), deg2rad(-10.411), deg2rad(94.495), deg2rad(-0.732);
		comau_comm.setJointPositions(pos);
	}
	pos_garra = false;
	comau_comm.Garra(pos_garra);
}

void positionCB(const geometry_msgs::Twist::ConstPtr& msg)
 {
    ROS_INFO("I heard: [%f %f %f]", msg->linear.x, msg->linear.y, msg->linear.z);
	
    posX = -msg->linear.x + 180;
    posY = msg->linear.z;
    posZ = - msg->linear.y + 560;
 }

void corrigeXYZ(){
   if (posZ < 1200)
	posZ = 1200;
   if(posX > 1300)
	posX = 1300; 
   if(posX < -1300)
	posX = -1300; 
   if(posY < 1500)
	posY = 1500;
   if(posY > 2000)
	posY = 2000;
}

/*********************************
*               MAIN             *
**********************************/

int main(int argc, char** argv)
{
	
    ros::init(argc, argv, "COMAU");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("/manip", 1, positionCB);
    
    ros::Rate loop_rate(1);

    example__initialPosition(argc,argv);

    QCoreApplication app(argc, argv);

    ComauComm comau_comm("172.22.121.2", 4121, 100);
    comau_comm.socketConnect();
    QByteArray response;
    comau_comm.getDataFromResponse(comau_comm.socketRead());

    comau_comm.setArmOverride(100);

    comau_comm.setTimeout(20000);

    VectorXd pos(6);

	VectorXd posBase(6);
    
    VectorXd posLinear(6);
    int i,i1;
    double *J;
    //double posX, posY, posZ;
    int ni;
    bool pos_garra = false;
    comau_comm.Garra(pos_garra);
	
	//As posi��es X Y e Z s�o as do alvo...
	posX = 0;
	posY = 1800;
	posZ = 1200;
	ni = 5;

  //  while (ros::ok())
  //  {

	//y = 1800
	//x = 1300
	//z = 1200
   	pos << deg2rad(90),0,-PI2,0,PI2,0;
   	comau_comm.setJointPositions(pos);
	for(i1=1;i1<5;i1++){
                //ros::Duration(2).sleep();
                //ros::spinOnce();
                pegadardo(i1, comau_comm);
                pos << deg2rad(90),0,-PI2,0,PI2,0;
   	        comau_comm.setJointPositions(pos);
                ros::Duration(2).sleep();
                ros::spinOnce();


		corrigeXYZ();
		
		//std::cout<<"Posicoe "<< posX);
		//std::cout<<"Posicoe "<< posY);
		//std::cout<<"Posicoe "<< posZ);

  		J = planejamento(posX,posY, posZ,ni);
  	
  		printpos(J, ni);

	 	comau_comm.setArmOverride(50);
  	
  		pos << J[0], J[1], J[2], J[3], J[4], J[5];
		comau_comm.setJointPositions(pos);
		comau_comm.setArmOverride(100);
		
		for (i = 0; i < ni; i++){
			pos << J[6*i], J[1 + 6*i], J[2 + 6*i], J[3 + 6*i], J[4 + 6*i], J[5 + 6*i];
			comau_comm.setJointPositionsFly(pos);
			if(i > ni/2) {
				pos_garra = true;
				comau_comm.Garra(pos_garra);
			}
		}
		pos_garra = false;
		comau_comm.Garra(pos_garra);
   		pos << deg2rad(90),0,-PI2,0,PI2,0;
   		comau_comm.setJointPositions(pos);
	}

/*
    pos << 0,deg2rad(-80),deg2rad(-60),0,deg2rad(-68),deg2rad(0);
	comau_comm.setJointPositions(pos);

    pos << 0,deg2rad(-23),deg2rad(-86),0,deg2rad(-68),deg2rad(0);
    comau_comm.setJointPositionsFly(pos);


   // pos << 0,deg2rad(10),deg2rad(-73),0,deg2rad(-68),deg2rad(0);
   // comau_comm.setJointPositionsFly(pos);
 
    pos << 0,deg2rad(45),deg2rad(-90),0,deg2rad(-68),deg2rad(0);
    comau_comm.setJointPositionsFly(pos);


    pos_garra = true;
    comau_comm.Garra(pos_garra);   

    ros::Duration(5).sleep();
    pos_garra = false;
    comau_comm.Garra(pos_garra);

*/
	//posBase = comau_comm.getBasePose();

	//posLinear<< posBase(0),posBase(1),posBase(2),deg2rad(180),0,deg2rad(-180);
    //comau_comm.tcpFlyMovement(posLinear);
/*
    posLinear<< -869.45,0,1119.352,3.141593,1.53589,2.356194;
    comau_comm.tcpFlyMovement(posLinear);
    
    posLinear<< 428.714,0,1497.331,3.141593,0.087266,2.356194;
    comau_comm.tcpFlyMovement(posLinear);

    posLinear<< 1229.485,0,749.502,0,0.907571,-0.785398;
    comau_comm.tcpFlyMovement(posLinear);


*/
    //pos << 0,0,-PI2,0,PI2,0;
    //comau_comm.setJointPositions(pos);
	
    //comau_comm.Garra(pos_garra);
    //pos_garra = !pos_garra;

   // ros::Duration(10).sleep();
    ros::spinOnce();

   // loop_rate.sleep();
    
  // }
	
    comau_comm.socketClose();
    printf("Fechou\n");

    return 0;  


}
