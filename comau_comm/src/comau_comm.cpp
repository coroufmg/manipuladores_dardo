/**
*
* Refer to comau_comm.hpp for further information.
*
************************************************************************
*
* Copyright (c) 2014, Murilo Marques Marinho
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
* 
* 1. Redistributions of source code must retain the above copyright notice, this
*    list of conditions and the following disclaimer. 
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
* 3. Neither the name of Murilo Marques Marinho nor the
*    names of its contributors may be used to endorse or promote products
*    derived from this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
* ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <comau_comm/comau_comm.h>
//#define COMAU_COMM_DEBUG

/*********************************
* Auxiliar Functions             *
**********************************/
double rad2deg(double rad)
{
    return(rad*180.0/PI);
}

double deg2rad(double deg)
{
    return(deg*PI/180.0);
}

/*********************************
* Socket Communications Function *
**********************************/

/**
* Connects to COMAU using the hostname and port
* used in the constructor.
* @returns false if there is a connection issue.
*/
bool ComauComm::socketConnect()
{
    socket_.connectToHost( hostname_ , port_ );
    return socket_.waitForConnected( timeout_ );
}

/**
* Waits for data to be readable and reads a connected socket buffer. It stops reading 
* when the server sends the last protocol line: "+\n".
*
* @returns the QByteArray with the server response.
*/
QByteArray ComauComm::socketRead()
{
    QByteArray response("");
    QByteArray chunk("");
    while( not response.contains("+\n") )
    {
        socket_.waitForReadyRead( 1 ); //This wait function seems to do more than just wait. Without it, the program doesn't work.
        chunk = socket_.readAll();     //This function has no specific way to report errors.
        response+=chunk;
    }
    return response;
}

/**
* Writes the specified QString to a connected socket.
* @param command The desired const char* to be sent.
* @returns false if there is a write error.
*/
bool ComauComm::socketWrite(const char* command)
{
    if(socket_.write(command) == -1) //write returns -1 when there is a write error.
        return false;
    else
    {
        socket_.flush();
        return true;
    }
}

/**
* Destroys the socket, closing the connection if necessary.
*/
void ComauComm::socketClose()
{
    socket_.close();
}


/**
* Function receives the response from the server as a QByteArray and obtains data from it,
* removing the validation characters and lines. 
* If there is any issue, a runtime_error exception will be thrown and should be treated
* by the calling thread.
*
* @param response The QByteArray response from the server.
*
* @return the part of the response with relevant data.
*
*/
QString ComauComm::getDataFromResponse(QByteArray response)
{
    QString qstr_response(response); //Transforms the response in a QString so that we can...
    QStringList response_lines = qstr_response.split("\n"); //...split it in lines easily.
    //The message ends with a "\n" string, so there will be a dataless last item on the QStringList using the given regex.
    response_lines.removeLast(); //So we remove it

    #ifdef COMAU_COMM_DEBUG
    std::cout << "Decoded QStringList:" << std::endl;
    for(int i=0;i<response_lines.size();i++)
        std::cout << response_lines.at(i).toLocal8Bit().constData() << std::endl;
    #endif

    //If the first character is a '-' it means the server received an Unknown command
    if(QString::compare(response_lines.at(0).at(0),"-")==0)
    {
        throw std::runtime_error("(e01) Server error: Unknown command.");
    }
    //If the first character is a '+', the command was understood.
    else if(QString::compare(response_lines.at(0).at(0),"+")==0)
    {
        QString response_data = response_lines.at(0);

        //The response can be a simple ack...
        if(response_lines.at(0).size() == 1)
            response_data.remove(0,1);
        //...or store relevant data.
        else
            response_data.remove(0,2); 

        #ifdef COMAU_COMM_DEBUG
        std::cout << "Decoded QString message:" << std::endl;
        std::cout << response_data.toLocal8Bit().constData() << std::endl;
        #endif
        
        return response_data;
    }
    //If the first character is something else, an invalid response was received.
    else
    {
        throw std::runtime_error("(e02) Decoder error: Received invalid response.");
    }


}

/*********************************
*        SET Methods             *
**********************************/

/**
* This function has been tested.
*/
void ComauComm::setArmOverride(int arm_override)
{
    QString command = QString::number(arm_override) + "\n";

    socketWrite("setArmOverride\n");
    socketWrite(command.toLocal8Bit().constData());
    
    getDataFromResponse(socketRead());   
}

/**
* This function has been tested.
*/
void ComauComm::setTimeout(int timeout)
{
    QString command = QString::number(timeout) + QString("\n");

    socketWrite("setTimeout\n");
    socketWrite(command.toLocal8Bit().constData());

    getDataFromResponse(socketRead());
}

/**
* Sends a new set of references in the robot joint space. The server
* <b>WILL BLOCK</b> other requests while the robot is moving.
*
* @param joint_vector the vector with the desired joint positions.
*
* @note This function has been tested.
*/
void ComauComm::setJointPositions(Matrix<double,6,1> joint_vector)
{
    QString command =  
    QString::number( rad2deg( joint_vector(0) ) ) + " " + 
    QString::number( rad2deg( joint_vector(1) ) ) + " " + 
    QString::number( rad2deg( joint_vector(2) ) ) + " " + 
    QString::number( rad2deg( joint_vector(3) ) ) + " " + 
    QString::number( rad2deg( joint_vector(4) ) ) + " " + 
    QString::number( rad2deg( joint_vector(5) ) ) + QString("\n");

    socketWrite("setJointPositions\n");
    socketWrite(command.toLocal8Bit().constData());

    getDataFromResponse(socketRead());
}

/**
* Sends a new set of references in the robot joint space. The server
* will <b>NOT<b> block other requests while the robot is moving.
*
* @param joint_vector the vector with the desired joint positions.
*
* @note This function has been tested.
*/
void ComauComm::setJointPositionsFly(Matrix<double,6,1> joint_vector)
{
    QString command =  
    QString::number( rad2deg( joint_vector(0) ) ) + " " + 
    QString::number( rad2deg( joint_vector(1) ) ) + " " + 
    QString::number( rad2deg( joint_vector(2) ) ) + " " + 
    QString::number( rad2deg( joint_vector(3) ) ) + " " + 
    QString::number( rad2deg( joint_vector(4) ) ) + " " + 
    QString::number( rad2deg( joint_vector(5) ) ) + QString("\n");

    socketWrite("setJointPositionsFly\n");
    socketWrite(command.toLocal8Bit().constData());

    getDataFromResponse(socketRead());
}
//Adicionar setArmPose e corrigir tcpFlyMovement

void ComauComm::setArmPose(Matrix<double,6,1> pos_vec)
{
    QString command =  
    QString::number(  pos_vec(0)  ) + " " + 
    QString::number(  pos_vec(1)  ) + " " + 
    QString::number(  pos_vec(2)  ) + " " + 
    QString::number(  rad2deg(pos_vec(3) ) ) + " " + 
    QString::number( rad2deg( pos_vec(4) ) ) + " " + 
    QString::number( rad2deg( pos_vec(5) ) ) + QString("\n");



    socketWrite("setArmPose\n");
    socketWrite(command.toLocal8Bit().constData());

    getDataFromResponse(socketRead());
}

void ComauComm::tcpFlyMovement(Matrix<double,6,1> pos_vec)
{
    QString command =  
    QString::number(  pos_vec(0)  ) + " " + 
    QString::number(  pos_vec(1)  ) + " " + 
    QString::number(  pos_vec(2)  ) + " " + 
    QString::number(  rad2deg(pos_vec(3) ) ) + " " + 
    QString::number( rad2deg( pos_vec(4) ) ) + " " + 
    QString::number( rad2deg( pos_vec(5) ) ) + QString("\n");



    socketWrite("flyMovement\n");
    socketWrite(command.toLocal8Bit().constData());

    getDataFromResponse(socketRead());
}


void ComauComm::Garra(bool pos)
{    
    if (pos)
      socketWrite("fechaGarra\n");
    else
      socketWrite("abreGarra\n");


    getDataFromResponse(socketRead());   
}

/*********************************
*        GET Methods             *
**********************************/

/**
* This function has been tested.
*/
Matrix<double,6,1> ComauComm::getJointPositions()
{
    Matrix<double,6,1> jointpositions;
    
    //Send request to server
    socketWrite("getJointPositions\n"); 

    //Receive and decode response
    QString data = getDataFromResponse(socketRead());
    data = data.simplified(); //has whitespace removed from the start and the end and each sequence of internal whitespace replaced with a single space

    //Decode data
    QStringList data_list = data.split(" ");

    for(unsigned int i=0;i<jointpositions.size();i++)
        jointpositions(i) = deg2rad( data_list.at(i).toDouble() );
        
    return jointpositions;
} 

Matrix<double,6,1> ComauComm::getBasePose()
{
    Matrix<double,6,1> basepose;
    
    //Send request to server
    socketWrite("getArmPose\n"); 

    //Receive and decode response
    QString data = getDataFromResponse(socketRead());
    data = data.simplified(); //has whitespace removed from the start and the end and each sequence of internal whitespace replaced with a single space

    //Decode data
    QStringList data_list = data.split(" ");

    for(unsigned int i=0;i<basepose.size();i++)
    {
        if(i<=2)
        	basepose(i) = data_list.at(i).toDouble();
	else
		basepose(i) = deg2rad( data_list.at(i).toDouble() );
    }
        
    return basepose;
} 

