#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include <ros/publisher.h>
#include <ros/init.h>
#include <ros/node_handle.h>
#include <std_msgs/Int8MultiArray.h>
#include "geometry_msgs/Twist.h"
#include <numeric>

using namespace cv;
using namespace std;

static const std::string OPENCV_WINDOW = "Image depth window";

static const std::string OPENCV_WINDOW2 = "Image RGB window";

int TAM_BUFFER = 10;
float c_x = 320;
float c_y = 240;
float f_x = 525;
float f_y = 525;
int z_w = 0;
int z = 0;
float px = 0;
float py = 0;
int cont = 0;

std::vector<float> bufferx(TAM_BUFFER);
//std::vector<float>::iterator ix = bufferx.begin();

std::vector<float> buffery(TAM_BUFFER);
//std::vector<float>::iterator iy = buffery.begin();

//int xyz_w[3];

class ImageConverter
{
  ros::NodeHandle nh_;
  ros::NodeHandle ph_;

  image_transport::ImageTransport it_;
  //geometry_msgs::Twist tw_;

  image_transport::Subscriber image_sub_;
  image_transport::Subscriber image_sub2_;

  image_transport::Publisher image_pub_;
  image_transport::Publisher image_pub2_;
  //Point pixel;
  Mat img_rgb;
  Mat img_depth;

  ros::Publisher xyz_pub;// = nh_.advertise<std_msgs::String>("manip", 10);

public:

  Point pixel;
  //ros::Publisher xyz_pub = nh_.advertise<geometry_msgs::Twist>("manip", 10);  

  ImageConverter(ros::NodeHandle &nh)
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/camera/depth_registered/image_rect_raw", 1, 
      &ImageConverter::imageCb, this);

    image_sub2_ = it_.subscribe("/camera/rgb/image_raw", 1, 
      &ImageConverter::imageCb2, this);


    image_pub_ = it_.advertise("out", 1);
    image_pub2_ = it_.advertise("out2", 1);
    //xyz_pub = it_.advertise<geometry_msgs::Twist>("manip", 10);

    ph_ = nh;
    xyz_pub = ph_.advertise<geometry_msgs::Twist>("manip", 10);

    cv::namedWindow(OPENCV_WINDOW);
    cv::namedWindow(OPENCV_WINDOW2);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(OPENCV_WINDOW);
    cv::destroyWindow(OPENCV_WINDOW2);
  }
  
  

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;

    try
    {
	cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_16UC1);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    Mat blur_img;
    geometry_msgs::Twist msgpb;

    // circle center
    circle( cv_ptr->image, pixel, 3, Scalar(0,255,0), -1, 8, 0 );
    
    // TESTE DO ALVO
    /*if (pixel.x  && pixel.y){    
	z = cv_ptr->image.at<short int>(cv::Point(pixel.y,pixel.x));
	int x = (pixel.x - c_x)*z_w/f_x;
	int y = (pixel.y - c_y)*z_w/f_y;
	cout<<"Xtemp : "<<x<<" "<<"Ytemp : "<<y<<" "<<"Ztemp : "<<z<<"\n";
    }*/
    // FIM DE TESTE (apagar)

    if (cont == TAM_BUFFER){
//	cout<<"AQUI3";
	//z_w = cv_ptr->image.at<short int>(cv::Point(py,px));
	z_w = 1600;
	cout<<"zw:"<<z_w<<"\n";
	//cout<<"Px: "<<px<<" Py: "<<py<<" Zed : "<<z_w<<"\n";		
	int x_w = (px - c_x)*z_w/f_x;
	int y_w = (py - c_y)*z_w/f_y;
	//cout<<"Real  x: "<<x_w<<" "<<"Real y: "<<y_w<<" "<<"Real Zed: "<<z_w<<"\n";

	// ROS PUBLISHER: Vetor para publicar com as 3 coordenadas (SINTAXE ERRADA)
			
	msgpb.linear.x = x_w;
	msgpb.linear.y = y_w;
	msgpb.linear.z = z_w;
	xyz_pub.publish(msgpb);
	cout<<"Publicado:"<<"\n"<<msgpb.linear<<"\n";

	//mtx.lock();
	//cont = 0;
	//mtx.unlock();
	
    }

    // IMPRESSAO DA JANELA DE DISTANCIA MISTA A RGB EM UMA JANELA VISIVEL PELO PROMPT
    double minVal, maxVal;
    //find minimum and maximum intensities
    minMaxLoc(cv_ptr->image, &minVal, &maxVal);
    //Mat draw;
    cv_ptr->image.convertTo(blur_img, CV_8U, 255.0/(maxVal - minVal), -minVal * 255.0/(maxVal - minVal));

    cv::imshow(OPENCV_WINDOW, blur_img);
  
    //cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    cv::waitKey(3);
    image_pub_.publish(cv_ptr->toImageMsg());  
  }
  
  void imageCb2(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;

    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
   
    img_rgb = cv_ptr->image;
     
    Mat src, src_gray; 
    
    src = cv_ptr->image;
    cvtColor( src, src_gray, CV_BGR2GRAY );

    /// Reduce the noise so we avoid false circle detection
    GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );

    vector<Vec3f> circles;

    /// Apply the Hough Transform to find the circles
    HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0 );

    /// Draw the circles detected
    for( size_t i = 0; i < circles.size(); i++ )
    {
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
	pixel = center;

	if (pixel.x  && pixel.y){
		for(int j=1; j<TAM_BUFFER;j++){
			bufferx[(TAM_BUFFER-j)] = bufferx[(TAM_BUFFER-j-1)];
			buffery[(TAM_BUFFER-j)] = buffery[(TAM_BUFFER-j-1)];
		}
		bufferx[0] = pixel.x/TAM_BUFFER;
		buffery[0] = pixel.y/TAM_BUFFER;
		if (cont<TAM_BUFFER){		
		cont ++;
		}
		cout<<"bufferx:"<<bufferx[0]<<"buffery:"<<buffery[0]<<"\n";
		//cout<<"CONTADOR: "<<cont;
		//cout<<"ix:"<<ix<<"iy:"<<iy<<"\n";
		//cout<<"bufferx:"<<bufferx<<"buffery:"<<buffery<<"\n";
		if (cont==TAM_BUFFER) {
			px = std::accumulate(bufferx.begin(), bufferx.end(), 0);
			py = std::accumulate(buffery.begin(), buffery.end(), 0);;
			cout<<"px:"<<px<<"py:"<<py<<"\n";		
			//cout<<"AQUI2";
		}	
		/*mtx.lock();		
		cont ++;
		mtx.unlock();		
		cout<<"Contador: "<<cont<<"\n";
		px += pixel.x/TAM_BUFFER;
		py += pixel.y/TAM_BUFFER;
*/
		/*++ix;
		++iy;
		cout<<"AQUI1";
		//cout<<"ix:"<<ix<<"iy:"<<iy<<"\n";
		//cout<<"bufferx:"<<bufferx<<"buffery:"<<buffery<<"\n";
		if (ix == bufferx.end() && iy == buffery.end()) {
			px = std::accumulate(bufferx.begin(), bufferx.end(), 0);
			py = std::accumulate(buffery.begin(), buffery.end(), 0);;		
			ix = bufferx.begin();
			iy = buffery.begin();
			cont = 1;
			cout<<"AQUI2";
		}
		*ix = pixel.x/TAM_BUFFER;
		*iy = pixel.y/TAM_BUFFER;*/
	}

        int radius = cvRound(circles[i][2]);
        // circle center
        circle( src, center, 3, Scalar(0,255,0), -1, 8, 0 );
        // circle outline
        circle( src, center, radius, Scalar(0,0,255), 3, 8, 0 );
    }

    /// Show your results
    //namedWindow( "Hough Circle Transform Demo", CV_WINDOW_AUTOSIZE );
    //imshow( "Hough Circle Transform Demo", src );
    cv_ptr->image = src;
    
    cv::imshow(OPENCV_WINDOW2, cv_ptr->image);
    cv::waitKey(3);
    image_pub2_.publish(cv_ptr->toImageMsg());
    
  }

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ros::NodeHandle nh;
  ImageConverter ic(nh);
  
  // ROS PUBLISHER
  /*
  ros::init(argc, argv, "publisher");
  ros::NodeHandle npb;
  ros::Publisher xyz_pub = npb.advertise<std_msgs::Int8MultiArray>("msg_pub", 10*TAM_BUFFER);
  ros::Rate rate(10.0);
  while (ros::ok())
  {
	std_msgs::Int8MultiArray msg_pub;
	if (cont == TAM_BUFFER){
		msg_pub.data.assign(xyz_w, xyz_w + 3);
		xyz_pub.publish(msg_pub);
		//cout<<"I Published: "<<xyz_w<<"\n";
		cont = 0;
	}
	//Do this.
	ros::spinOnce();
	//Added a delay so not to spam
	rate.sleep();
  }
  */

  cout<<ic.pixel.x;

  ros::spin();
  return 0;
}

